use aoc_runner_derive::*;

use std::collections::{HashMap, HashSet};

#[aoc_generator(day8)]
fn gen(input: &str) -> Vec<(Vec<HashSet<String>>, Vec<HashSet<String>>)> {
    input
        .lines()
        .map(|line| {
            let ref mut parts = line.split(" | ").collect::<Vec<&str>>();
            (
                parts[0]
                    .split(" ")
                    .map(|s| {
                        s.split("")
                            .map(|c| c.to_string())
                            .filter_map(|c| match c.is_empty() {
                                false => Some(c.to_string()),
                                _ => None,
                            })
                            .collect::<HashSet<String>>()
                    })
                    .collect::<Vec<HashSet<String>>>(),
                parts[1]
                    .split(" ")
                    .map(|s| {
                        s.split("")
                            .filter_map(|c| match c.is_empty() {
                                false => Some(c.to_string()),
                                _ => None,
                            })
                            .collect::<HashSet<String>>()
                    })
                    .collect::<Vec<HashSet<String>>>(),
            )
        })
        .collect()
}

fn guess(val: &HashSet<String>) -> Option<usize> {
    match val.len() {
        2 => Some(1),
        4 => Some(4),
        3 => Some(7),
        7 => Some(8),
        _ => None,
    }
}

fn deduce(pats: &Vec<HashSet<String>>) -> HashMap<usize, HashSet<String>> {
    let mut res: HashMap<usize, HashSet<_>> = HashMap::new();
    let mut pats = pats.to_vec();

    let clone = |hs: &HashSet<String>| hs.iter().map(|i| i.clone()).collect::<HashSet<String>>();

    let mut i = 0;

    while pats.len() > 0 {
        for i in (0..pats.len()).rev() {
            match guess(&pats[i]) {
                Some(v) => {
                    res.insert(v, clone(&pats[i]));
                    pats.remove(i);
                }
                None => match pats[i].len() {
                    5 => {
                        if !res.contains_key(&3)
                            && res.contains_key(&7)
                            && (&pats[i]).intersection(res.get(&7).unwrap()).count() == 3
                        {
                            res.insert(3, clone(&pats[i]));
                            pats.remove(i);
                        } else if res.contains_key(&3) && res.contains_key(&4) {
                            let num = match (&pats[i]).intersection(res.get(&4).unwrap()).count() {
                                3 => 5,
                                2 => 2,
                                _ => panic!("This shouldn't happen"),
                            };
                            res.insert(num, clone(&pats[i]));
                            pats.remove(i);
                        }
                    }
                    6 => {
                        if !res.contains_key(&9)
                            && res.contains_key(&4)
                            && (&pats[i]).intersection(res.get(&4).unwrap()).count() == 4
                        {
                            res.insert(9, clone(&pats[i]));
                            pats.remove(i);
                        } else if !res.contains_key(&0)
                            && res.contains_key(&9)
                            && res.contains_key(&1)
                            && (&pats[i]).intersection(res.get(&1).unwrap()).count() == 2
                        {
                            res.insert(0, clone(&pats[i]));
                            pats.remove(i);
                        } else if !res.contains_key(&6)
                            && res.contains_key(&0)
                            && res.contains_key(&9)
                        {
                            res.insert(6, clone(&pats[i]));
                            pats.remove(i);
                        }
                    }
                    _ => {}
                },
            }
        }
        i += 1;
        /*
        if i > 3 {
            println!("This is what we had: {:?}", res.keys());
            panic!(
                "This shouldn't happen. There seem to be undefined {} symbols left:\n{:#?}",
                pats.len(),
                pats
            );
        }*/
    }
    res
}

#[aoc(day8, part1)]
fn part1(input: &[(Vec<HashSet<String>>, Vec<HashSet<String>>)]) -> usize {
    let mut count: usize = 0;
    for (_patterns, values) in input.iter() {
        count += values.iter().map(guess).filter_map(|o| o).count();
    }
    count
}

#[aoc(day8, part2)]
fn part2(input: &[(Vec<HashSet<String>>, Vec<HashSet<String>>)]) -> usize {
    let mut count = 0;
    for (patterns, values) in input.iter() {
        let map = deduce(patterns);

        for (i, value) in values.iter().rev().enumerate() {
            count += match map
                .iter()
                .find_map(|(key, val)| if val == value { Some(key) } else { None })
            {
                Some(num) => num,
                _ => panic!("{:?} not found", value),
            } * (10_usize.pow(i as u32));
        }
    }
    count
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

    #[test]
    fn part1() {
        assert_eq!(26, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(61229, solve_part2(&gen(EXAMPLE)));
    }
}
