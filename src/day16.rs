use aoc_runner_derive::*;

type Format = Vec<usize>;

#[aoc_generator(day16)]
fn gen(input: &str) -> Format {
    Format::new()
}

#[aoc(day16, part1)]
fn part1(input: &Format) -> usize {
    0
}

#[aoc(day16, part2)]
fn part2(input: &Format) -> usize {
    0
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"";

    #[test]
    fn part1() {
        assert_eq!(1, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(1, solve_part2(&gen(EXAMPLE)));
    }
}
