use aoc_runner_derive::*;

type Format = Vec<Vec<String>>;

#[aoc_generator(day10)]
fn gen(input: &str) -> Format {
    input
        .lines()
        .map(|line| {
            line.split("")
                .filter(|c| !c.is_empty())
                .map(|c| c.to_string())
                .collect()
        })
        .collect()
}

fn get_pair(paren: &String) -> String {
    let paren = paren.as_str();
    match paren {
        "(" => ")",
        "[" => "]",
        "{" => "}",
        "<" => ">",
        _ => "-",
    }
    .to_string()
}

fn score_line_syntax(line: &Vec<String>) -> usize {
    let lefts: Vec<&str> = vec!["[", "(", "{", "<"];
    let mut stack: Vec<String> = vec![];
    let mut point: usize = 0;
    for c in line {
        if lefts.contains(&c.as_str()) {
            stack.push(c.to_string());
        } else if !stack.is_empty() && get_pair(stack.last().unwrap()).to_string() == *c {
            stack.pop();
        } else {
            point = match c.as_str() {
                ")" => 3,
                "]" => 57,
                "}" => 1197,
                ">" => 25137,
                _ => panic!("Unexpected symbol {:?}", c),
            };
            break;
        }
    }

    return point;
}

#[aoc(day10, part1)]
fn part1(input: &Format) -> usize {
    input.iter().map(|line| score_line_syntax(line)).sum()
}

#[aoc(day10, part2)]
fn part2(input: &Format) -> usize {
    let mut ans = input
        .iter()
        .filter(|line| score_line_syntax(line) == 0)
        .map(|line| {
            let lefts: Vec<&str> = vec!["[", "(", "{", "<"];
            let mut stack: Vec<String> = vec![];
            for c in line {
                if lefts.contains(&c.as_str()) {
                    stack.push(c.to_string());
                } else if !stack.is_empty() && get_pair(stack.last().unwrap()).to_string() == *c {
                    stack.pop();
                }
            }
            stack
                .iter()
                .rev()
                .map(|c| match c.as_str() {
                    "[" => 2,
                    "(" => 1,
                    "{" => 3,
                    "<" => 4,
                    _ => 0,
                })
                .fold(0, |acc, x| (acc * 5) + x)
        })
        .collect::<Vec<_>>();
    ans.sort();

    ans[ans.len() / 2]
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

    #[test]
    fn part1() {
        assert_eq!(26397, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(288957, solve_part2(&gen(EXAMPLE)));
    }
}
