use aoc_runner_derive::*;

use std::collections::{HashMap, HashSet};

type Point = usize;

#[aoc_generator(day9)]
fn gen(input: &str) -> Vec<((usize, usize), Point)> {
    input
        .lines()
        .enumerate()
        .map(|(i, line)| {
            line.split("")
                .filter_map(|c| if c.is_empty() { None } else { Some(c) })
                .enumerate()
                .map(move |(j, c)| ((i, j), c.parse::<usize>().unwrap()))
        })
        .flatten()
        .map(|(k, v)| (k, v))
        .collect()
}

fn neighbours((x, y): (usize, usize)) -> HashSet<(usize, usize)> {
    vec![
        ((false, 1_usize), (false, 0_usize)),
        ((true, 1_usize), (false, 0_usize)),
        ((false, 0_usize), (false, 1_usize)),
        ((false, 0_usize), (true, 1_usize)),
    ]
    .iter()
    .filter_map(|loc| match loc {
        ((true, vx), (_, vy)) => x.checked_sub(*vx).and_then(|nx| Some((nx, vy + y))),
        ((_, vx), (true, vy)) => y.checked_sub(*vy).and_then(|ny| Some((vx + x, ny))),
        ((_, vx), (_, vy)) => Some((vx + x, vy + y)),
    })
    .collect()
}

fn min_neighbour(here: (usize, usize), data: &HashMap<(usize, usize), Point>) -> &usize {
    neighbours(here)
        .iter()
        .filter_map(|loc| data.get(&loc))
        .min()
        .unwrap()
}

fn get_low_points(input: HashMap<(usize, usize), Point>) -> Vec<((usize, usize), usize)> {
    input
        .iter()
        .filter_map(|(k, v)| {
            if min_neighbour(*k, &input) >= v {
                Some((*k, *v))
            } else {
                None
            }
        })
        .collect()
}

#[aoc(day9, part1)]
fn part1(input: &Vec<((usize, usize), Point)>) -> usize {
    let input: HashMap<(usize, usize), Point> = input.iter().map(|e| *e).collect();
    get_low_points(input).iter().map(|(k, v)| v + 1).sum()
}

#[aoc(day9, part2)]
fn part2(input: &Vec<((usize, usize), Point)>) -> usize {
    let data: HashMap<(usize, usize), Point> = input.iter().map(|e| *e).collect();

    let mut starts: HashSet<(usize, usize)> = get_low_points(input.iter().map(|v| *v).collect())
        .iter()
        .map(|(k, v)| *k)
        .collect();
    let mut done: HashSet<(usize, usize)> = HashSet::new();

    let mut basins: Vec<HashSet<(usize, usize)>> = vec![];

    fn pop(set: &mut HashSet<(usize, usize)>) -> (usize, usize) {
        let any = set.iter().next().unwrap();
        let any = *any;
        set.take(&any).unwrap()
    }

    while !starts.is_empty() {
        let start = pop(&mut starts);
        //new_basin
        let mut basin: HashSet<(usize, usize)> = HashSet::new();
        let mut queue: HashSet<(usize, usize)> = HashSet::new();
        queue.insert(start);
        basin.insert(start);
        while !queue.is_empty() {
            let cur = pop(&mut queue);
            let curv = data.get(&cur).unwrap();
            done.insert(cur);
            for n in neighbours(cur) {
                if !done.contains(&n)
                    && data
                        .get(&n)
                        .and_then(|nv| Some(nv >= curv && nv != &9))
                        .unwrap_or_else(|| false)
                {
                    queue.insert(n);
                    basin.insert(n);
                }
            }
        }
        println!("{:?}", basin);
        starts = starts
            .difference(&basin)
            .map(|v| *v)
            .clone()
            .collect::<HashSet<_>>();

        //Todo starts -= new_basin
        basins.push(basin);
    }

    basins.sort_by_key(|ba| ba.len());
    basins.iter().rev().map(|ba| ba.len()).take(3).product()
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"2199943210
3987894921
9856789892
8767896789
9899965678";

    #[test]
    fn part1() {
        assert_eq!(15, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(1134, solve_part2(&gen(EXAMPLE)));
    }
}
