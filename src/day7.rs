use aoc_runner_derive::*;
use std::cmp::{max, min};

#[aoc_generator(day7)]
fn gen(input: &str) -> Vec<usize> {
    input
        .split(",")
        .filter_map(|o| {
            if !o.is_empty() {
                o.trim().parse::<usize>().ok()
            } else {
                None
            }
        })
        .collect::<Vec<usize>>()
}

pub fn sigma(num: usize) -> usize {
    (0..=num).sum()
}

#[aoc(day7, part1)]
fn part1(input: &[usize]) -> usize {
    let calc = |point: usize| {
        input
            .iter()
            .map(|c| max(c, &point) - min(c, &point))
            .sum::<usize>()
    };
    (*input.iter().min().unwrap()..=*input.iter().max().unwrap())
        .map(calc)
        .min()
        .unwrap()
}

#[aoc(day7, part2)]
fn part2(input: &[usize]) -> usize {
    let calc = |point: usize| {
        input
            .iter()
            .map(|c| sigma(max(c, &point) - min(c, &point)))
            .sum::<usize>()
    };
    (*input.iter().min().unwrap()..=*input.iter().max().unwrap())
        .map(calc)
        .min()
        .unwrap()
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"16,1,2,0,4,2,7,1,2,14";

    #[test]
    fn part1() {
        assert_eq!(37, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(168, solve_part2(&gen(EXAMPLE)));
    }
}
