use aoc_runner_derive::*;

use std::collections::{HashMap, HashSet, VecDeque};

#[aoc_generator(day12)]
fn gen(input: &str) -> Vec<(String, String)> {
    input
        .lines()
        .map(|line| {
            let mut parts = line.split("-").filter_map(|elem| {
                if elem.is_empty() {
                    None
                } else {
                    Some(elem.to_string())
                }
            });
            return (parts.next().unwrap(), parts.next().unwrap());
        })
        .collect()
}

#[aoc(day12, part1)]
fn part1(input: &[(String, String)]) -> usize {
    let mut graph: HashMap<&str, HashSet<&str>> = HashMap::new();
    let mut paths: Vec<Vec<&str>> = Vec::new();
    let mut queue: VecDeque<Vec<&str>> = VecDeque::new();
    queue.push_back(vec!["start"]);
    for (a, b) in input.iter() {
        let n = graph.entry(a.as_str()).or_insert(HashSet::new());
        n.insert(&b.as_str());
        let n = graph.entry(b.as_str()).or_insert(HashSet::new());
        n.insert(&a.as_str());
    }
    while let Some(path) = queue.pop_front() {
        let node = path.last().unwrap();
        let new_paths = graph
            .get(node)
            .and_then(|tos| {
                let new_paths = tos
                    .iter()
                    .filter_map(|to| {
                        if to.to_lowercase().as_str() == *to && path.contains(to) {
                            None
                        } else {
                            let mut new_path = path.to_vec();
                            new_path.push(to);
                            Some(new_path)
                        }
                    })
                    .collect::<Vec<Vec<&str>>>();
                match new_paths.len() {
                    0 => None,
                    _ => Some(new_paths),
                }
            })
            .unwrap_or(vec![]);
        if new_paths.is_empty() {
            // This is path is a dead-end
        } else {
            for path in new_paths {
                if path.last().and_then(|e| Some(e == &"end")).unwrap_or(false) {
                    paths.push(path);
                } else {
                    queue.push_back(path);
                }
            }
        }
    }
    paths.len()
}

#[aoc(day12, part2)]
fn part2(input: &[(String, String)]) -> usize {
    let mut graph: HashMap<&str, HashSet<&str>> = HashMap::new();
    let mut paths: Vec<Vec<&str>> = Vec::new();
    for (a, b) in input.iter() {
        let n = graph.entry(a.as_str()).or_insert(HashSet::new());
        n.insert(&b.as_str());
        let n = graph.entry(b.as_str()).or_insert(HashSet::new());
        n.insert(&a.as_str());
    }

    for special in graph
        .keys()
        .filter(|&name| name.to_lowercase().as_str() == *name && name != &"start" && name != &"end")
    {
        println!("Special: {}", special);
        let mut queue: VecDeque<Vec<&str>> = VecDeque::new();
        queue.push_back(vec!["start"]);
        while let Some(path) = queue.pop_front() {
            let node = path.last().unwrap();
            let new_paths = graph
                .get(node)
                .and_then(|tos| {
                    let new_paths = tos
                        .iter()
                        .filter_map(|to| {
                            if to.to_lowercase().as_str() == *to
                                && path.contains(to)
                                && !(to == special
                                    && path.iter().filter(|&elem| elem == special).count() < 2)
                            {
                                None
                            } else {
                                let mut new_path = path.to_vec();
                                new_path.push(to);
                                Some(new_path)
                            }
                        })
                        .collect::<Vec<Vec<&str>>>();
                    match new_paths.len() {
                        0 => None,
                        _ => Some(new_paths),
                    }
                })
                .unwrap_or(vec![]);
            if new_paths.is_empty() {
                // This is path is a dead-end
            } else {
                for path in new_paths {
                    if path.last().and_then(|e| Some(e == &"end")).unwrap_or(false) {
                        paths.push(path);
                    } else {
                        queue.push_back(path);
                    }
                }
            }
        }
    }
    paths.sort();
    paths.dedup();
    paths.len()
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"start-A
start-b
A-c
A-b
b-d
A-end
b-end";

    #[test]
    fn part1() {
        assert_eq!(10, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(36, solve_part2(&gen(EXAMPLE)));
    }
}
