use aoc_runner_derive::*;

use pathfinding::prelude::dijkstra;
use std::collections::HashMap;

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Pos(usize, usize);

impl Pos {
    fn successors(&self, board: &HashMap<Pos, usize>) -> Vec<(Pos, usize)> {
        let &Pos(x, y) = self;
        vec![
            (Some(true), None),
            (Some(false), None),
            (None, Some(true)),
            (None, Some(false)),
        ]
        .iter()
        .filter_map(|(a, b)| match (a, b) {
            (Some(a), None) => {
                (if *a { Some(x + 1) } else { x.checked_sub(1) }).and_then(|x| Some(Pos(x, y)))
            }
            (None, Some(b)) => {
                (if *b { Some(y + 1) } else { y.checked_sub(1) }).and_then(|y| Some(Pos(x, y)))
            }
            _ => None,
        })
        .filter_map(|p| match board.get(&p) {
            Some(c) => Some((p, *c)),
            _ => None,
        })
        .collect()
    }
}

#[aoc_generator(day15)]
fn gen(input: &str) -> HashMap<Pos, usize> {
    input
        .lines()
        .enumerate()
        .map(|(y, line)| {
            line.chars()
                .enumerate()
                .map(move |(x, c)| (Pos(x, y), c.to_digit(10).unwrap() as usize))
        })
        .flatten()
        .collect::<HashMap<Pos, usize>>()
}

#[aoc(day15, part1)]
fn part1(input: &HashMap<Pos, usize>) -> usize {
    let GOAL: Pos = input.keys().max().unwrap().clone();
    let START: Pos = Pos(0, 0);
    let result = dijkstra(&START, |p| p.successors(&input), |p| *p == GOAL);
    result.expect("no path found").1
}

#[aoc(day15, part2)]
fn part2(input_base: &HashMap<Pos, usize>) -> usize {
    let mut input: HashMap<Pos, usize> = HashMap::new();
    let max = input_base.keys().max().unwrap().clone();

    for xm in 0..5 {
        for ym in 0..5 {
            for (Pos(x, y), c) in input_base {
                input.insert(
                    Pos(*x + (xm * (max.0 + 1)), *y + (ym * (max.1 + 1))),
                    (c + xm + ym - 1) % 9 + 1,
                );
            }
        }
    }
    part1(&input)
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581";

    #[test]
    fn part1() {
        assert_eq!(40, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(315, solve_part2(&gen(EXAMPLE)));
    }
}
