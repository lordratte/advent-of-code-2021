use aoc_runner_derive::*;

use std::collections::HashSet;

#[aoc_generator(day13)]
fn gen(input: &str) -> (Vec<(usize, usize)>, Vec<(bool, usize)>) {
    (
        input
            .lines()
            .map(|line| line.split(",").map(|e| e.parse::<usize>()))
            .filter_map(|mut elems| match (elems.next(), elems.next()) {
                (Some(Ok(a)), Some(Ok(b))) => Some((a, b)),
                _ => None,
            })
            .collect(),
        input
            .lines()
            .filter_map(|line| {
                let mut split = line.split("=");
                match (
                    split.next().and_then(|e| Some(e.contains("x"))),
                    split.last().and_then(|n| n.parse::<usize>().ok()),
                ) {
                    (Some(a), Some(b)) => Some((a, b)),
                    _ => None,
                }
            })
            .collect(),
    )
}

fn print(marks: &HashSet<(usize, usize)>) {
    for y in 0..=(*marks.iter().map(|(_, y)| y).max().unwrap()) {
        for x in 0..=(*marks.iter().map(|(x, _)| x).max().unwrap()) {
            if marks.contains(&(x, y)) {
                print!("{}", "#");
            } else {
                print!("{}", ".");
            }
        }
        print!("\n");
    }
    print!("\n");
}

#[aoc(day13, part1)]
fn part1((marks, folds): &(Vec<(usize, usize)>, Vec<(bool, usize)>)) -> usize {
    let mut marks: HashSet<(usize, usize)> = marks.iter().cloned().collect();
    //print(&marks);
    for (left, size) in folds.iter().take(1) {
        marks = if *left {
            marks
                .iter()
                .map(|(x, y)| (if x > size { 2 * size - x } else { *x }, *y))
                .collect()
        } else {
            marks
                .iter()
                .map(|(x, y)| (*x, if y > size { 2 * size - y } else { *y }))
                .collect()
        };
        //print(&marks);
    }
    marks.len()
}

#[aoc(day13, part2)]
fn part2((marks, folds): &(Vec<(usize, usize)>, Vec<(bool, usize)>)) -> bool {
    let mut marks: HashSet<(usize, usize)> = marks.iter().cloned().collect();
    //print(&marks);
    for (left, size) in folds {
        marks = if *left {
            marks
                .iter()
                .map(|(x, y)| (if x > size { 2 * size - x } else { *x }, *y))
                .collect()
        } else {
            marks
                .iter()
                .map(|(x, y)| (*x, if y > size { 2 * size - y } else { *y }))
                .collect()
        };
    }
    print(&marks);
    true
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";

    #[test]
    fn part1() {
        assert_eq!(17, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(true, solve_part2(&gen(EXAMPLE)));
    }
}
