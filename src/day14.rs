use aoc_runner_derive::*;

use std::collections::{HashMap, HashSet};

#[aoc_generator(day14)]
fn gen(input: &str) -> (Vec<char>, Vec<((char, char), char)>) {
    let mut i = input.lines();
    (
        i.next().expect("The first line").chars().collect(),
        i.filter_map(|line| {
            let mut split = line.split(" -> ");
            match (split.next(), split.next()) {
                (Some(a), Some(b)) => match &a.chars().take(2).collect::<Vec<char>>()[..] {
                    &[first, second, ..] => Some(((first, second), b.chars().next().unwrap())),
                    _ => None,
                },
                _ => None,
            }
        })
        .collect(),
    )
}

fn generate(
    (template, pairs): &(Vec<char>, Vec<((char, char), char)>),
    iterations: usize,
) -> usize {
    let pairs: HashMap<(char, char), char> = pairs.iter().cloned().collect();
    let mut new: Vec<char> = template.iter().cloned().collect();

    for _ in 0..iterations {
        new = new
            .windows(2)
            .map(|part| match part {
                &[f, s, ..] => {
                    if let Some(m) = pairs.get(&(f, s)) {
                        vec![f, *m]
                    } else {
                        vec![f]
                    }
                }
                &[f, ..] => vec![f],
                _ => panic!("Empty window"),
            })
            .flatten()
            .chain(template.iter().rev().take(1).map(|c| *c))
            .collect();
        println!("{}", new.iter().collect::<String>());
    }
    let counts = new
        .iter()
        .collect::<HashSet<_>>()
        .iter()
        .map(|c| new.iter().filter(|a| a == c).count())
        .collect::<HashSet<usize>>();
    counts.iter().max().unwrap() - counts.iter().min().unwrap()
}

#[aoc(day14, part1)]
fn part1(input: &(Vec<char>, Vec<((char, char), char)>)) -> usize {
    generate(input, 10)
}

struct Chunk {
    seq: Vec<char>,
}

#[aoc(day14, part2)]
fn part2((template, pairs): &(Vec<char>, Vec<((char, char), char)>)) -> usize {
    return 0;
    let pairs: HashMap<(char, char), char> = pairs.iter().cloned().collect();
    let mut new: Vec<char> = template.iter().cloned().collect();

    for _ in 0..10 {
        new = new
            .windows(2)
            .map(|part| match part {
                &[f, s, ..] => {
                    if let Some(m) = pairs.get(&(f, s)) {
                        vec![f, *m]
                    } else {
                        vec![f]
                    }
                }
                &[f, ..] => vec![f],
                _ => panic!("Empty window"),
            })
            .flatten()
            .chain(template.iter().rev().take(1).map(|c| *c))
            .collect();
        //println!("{}", new.iter().collect::<String>());
    }
    let counts = new
        .iter()
        .collect::<HashSet<_>>()
        .iter()
        .map(|c| new.iter().filter(|a| a == c).count())
        .collect::<HashSet<usize>>();
    counts.iter().max().unwrap() - counts.iter().min().unwrap()
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

    #[test]
    fn part1() {
        assert_eq!(1588, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(2188189693529, solve_part2(&gen(EXAMPLE)));
    }
}
