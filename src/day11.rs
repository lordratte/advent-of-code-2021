use aoc_runner_derive::*;

#[aoc_generator(day11)]
fn gen(input: &str) -> Vec<Vec<u8>> {
    input
        .lines()
        .map(|line| {
            line.split("")
                .filter(|c| !c.is_empty())
                .map(|c| c.parse::<u8>().unwrap())
                .collect()
        })
        .collect()
}

#[aoc(day11, part1)]
fn part1(input: &[Vec<u8>]) -> usize {
    let mut total = 0;
    let mut map: Vec<u8> = input.iter().flatten().map(|e| *e).collect();
    let w = input[0].len();
    for _s in 1..=100 {
        let mut resets = vec![false; map.len()];
        for o in 0..=map.len() {
            for i in 0..map.len() {
                if o == 0 {
                    map[i] += 1;
                } else {
                    if map[i] <= 9 || resets[i] {
                        continue;
                    }
                    resets[i] = true;
                    let mut safe_left = false;
                    let mut safe_right = false;
                    if (i % w) > 0 {
                        map[i - 1] += 1;
                        safe_left = true;
                    }
                    if (i % w) < (w - 1) {
                        map[i + 1] += 1;
                        safe_right = true;
                    }
                    if i >= w {
                        map[i - w] += 1;
                        if safe_left {
                            map[i - w - 1] += 1;
                        }
                        if safe_right {
                            map[i - w + 1] += 1;
                        }
                    }
                    if map.len() > (i + w) {
                        map[i + w] += 1;
                        if safe_left {
                            map[i + w - 1] += 1;
                        }
                        if safe_right {
                            map[i + w + 1] += 1;
                        }
                    }
                }
            }
        }

        for i in 0..map.len() {
            if resets[i] {
                map[i] = 0;
                total += 1;
            }
        }
    }

    total
}

#[aoc(day11, part2)]
fn part2(input: &[Vec<u8>]) -> usize {
    let mut map: Vec<u8> = input.iter().flatten().map(|e| *e).collect();
    let w = input[0].len();
    let mut sync: Option<usize>;
    let mut s = 0;
    loop {
        s += 1;
        let mut resets = vec![false; map.len()];
        for o in 0..=map.len() {
            for i in 0..map.len() {
                if o == 0 {
                    map[i] += 1;
                } else {
                    if map[i] <= 9 || resets[i] {
                        continue;
                    }
                    resets[i] = true;
                    let mut safe_left = false;
                    let mut safe_right = false;
                    if (i % w) > 0 {
                        map[i - 1] += 1;
                        safe_left = true;
                    }
                    if (i % w) < (w - 1) {
                        map[i + 1] += 1;
                        safe_right = true;
                    }
                    if i >= w {
                        map[i - w] += 1;
                        if safe_left {
                            map[i - w - 1] += 1;
                        }
                        if safe_right {
                            map[i - w + 1] += 1;
                        }
                    }
                    if map.len() > (i + w) {
                        map[i + w] += 1;
                        if safe_left {
                            map[i + w - 1] += 1;
                        }
                        if safe_right {
                            map[i + w + 1] += 1;
                        }
                    }
                }
            }
        }

        sync = Some(s);
        for i in 0..map.len() {
            if resets[i] {
                map[i] = 0;
            } else {
                sync = None;
            }
        }
        if sync.is_some() {
            break;
        }
    }
    sync.unwrap()
}

#[cfg(test)]
mod test {
    use super::{gen, part1 as solve_part1, part2 as solve_part2};

    const EXAMPLE: &str = r"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    #[test]
    fn part1() {
        assert_eq!(1656, solve_part1(&gen(EXAMPLE)));
    }

    #[test]
    fn part2() {
        assert_eq!(195, solve_part2(&gen(EXAMPLE)));
    }
}
