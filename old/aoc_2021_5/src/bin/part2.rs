use std::cmp::{min,max};
use std::hash::{Hash};
use std::collections::HashMap;

mod input;

#[derive(Debug, PartialEq)]
enum Orient {
    Horizontal,
    Vertical,
    Diagonal
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
struct Point {
    x: usize,
    y: usize
}

#[derive(Debug)]
struct Line {
    x1: usize,
    x2: usize,
    y1: usize,
    y2: usize,
    orient: Orient,
    points : Option<Vec<Point>>,
    delta: (bool, bool)
}

impl Line {
    pub fn new(point1 : &str, point2 : &str) -> Self {
        let p1 : Vec<usize> = point1.split(",").map(|n| n.parse::<usize>().unwrap()).collect();
        let p2 : Vec<usize> = point2.split(",").map(|n| n.parse::<usize>().unwrap()).collect();
        let orient : Orient;
        if &p1[0] == &p2[0] {
            orient = Orient::Vertical;
        } else if &p1[1] == &p2[1] {
            orient = Orient::Horizontal;
        } else {
            orient = Orient::Diagonal;
        }
        Self {
                x1:p1[0], y1:p1[1],
                x2:p2[0], y2:p2[1],
                orient,
                points: None,
                delta: ((p2[0] as isize)-(p1[0] as isize) > 0, (p2[1] as isize)-(p1[1] as isize) > 0)
             }
    }

    fn get_points(&self) -> Vec<Point> {
        match self.orient {
            Orient::Horizontal => {
                (min(self.x1, self.x2)..=max(self.x1, self.x2)).map(|x| Point {x:x, y:self.y1}).collect()
            }
            Orient::Vertical => {
                (min(self.y1, self.y2)..=max(self.y1, self.y2)).map(|y| Point {x:self.x1, y:y}).collect()
            }
            Orient::Diagonal=> {
                let rangex = match self.delta.0 {
                                    false => (min(self.x1, self.x2)..=max(self.x1,self.x2)).rev().collect::<Vec<usize>>(),
                                    _ => (min(self.x1, self.x2)..=max(self.x1,self.x2)).collect::<Vec<usize>>()
                                    };
                let rangey = match self.delta.1 { 
                                    false => (min(self.y1, self.y2)..=max(self.y1,self.y2)).rev().collect::<Vec<usize>>(),
                                    _ => (min(self.y1, self.y2)..=max(self.y1,self.y2)).collect::<Vec<usize>>()
                                    };
                rangex.iter()
                    .zip(rangey.iter())
                    .map(|(a, b)| Point { x: *a, y: *b } )
                    .collect::<Vec<Point>>()
            }
        }
    }

    pub fn points(&mut self) -> Vec<Point> {
        match self.points {
            None => {
                self.points = Some(self.get_points())
            },
            _ => ()
        }
        let points = self.points.as_ref();
        points.unwrap().to_vec()
    }
}

fn main() {
    let lines = input::lines();

    let mut lines : Vec<Line> = lines.filter(|line| !line.as_ref().unwrap().trim().is_empty())
                                .map(|line| line.unwrap().to_string())
                                .map(|line| line.split(" -> ").filter(|seg| !seg.trim().is_empty())
                                                              .map(|seg| seg.to_string())
                                                              .collect::<Vec<String>>())
                                .map(|vals| Line::new(vals[0].as_str(), vals[1].as_str()) )
                                .collect();
    let mut points = HashMap::new();
    for (_i, line) in lines.iter_mut().enumerate() {
        for p in line.points() {
            if let Some(x) = points.get_mut(&p) {
                *x += 1;
            } else {
                points.insert(p, 1);
            }
        }

    }

    println!("{:#?}", points.into_values().filter(|x| *x>1).count());
}
