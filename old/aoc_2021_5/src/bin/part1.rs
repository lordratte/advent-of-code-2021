use std::cmp::{min,max};
use std::collections::HashSet;
use std::iter::FromIterator;
use std::hash::{Hash, Hasher};

mod input;

#[derive(Debug, PartialEq)]
enum Orient {
    Horizontal,
    Vertical,
    Diagonal
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
struct Point {
    x: usize,
    y: usize
}

#[derive(Debug)]
struct Line {
    x1: usize,
    x2: usize,
    y1: usize,
    y2: usize,
    orient: Orient,
    points : Option<Vec<Point>>
}

impl Line {
    pub fn new(point1 : &str, point2 : &str) -> Self {
        let p1 : Vec<usize> = point1.split(",").map(|n| n.parse::<usize>().unwrap()).collect();
        let p2 : Vec<usize> = point2.split(",").map(|n| n.parse::<usize>().unwrap()).collect();
        let orient : Orient;
        if &p1[0] == &p2[0] {
            orient = Orient::Vertical;
        } else if &p1[1] == &p2[1] {
            orient = Orient::Horizontal;
        } else {
            orient = Orient::Diagonal;
        }
        Self {
                x1:p1[0], y1:p1[1],
                x2:p2[0], y2:p2[1],
                orient,
                points: None
             }
    }

    fn get_points(&self) -> Vec<Point> {
        match self.orient {
            Orient::Horizontal => {
                (min(self.x1, self.x2)..=max(self.x1, self.x2)).map(|x| Point {x:x, y:self.y1}).collect()
            }
            Orient::Vertical => {
                (min(self.y1, self.y2)..=max(self.y1, self.y2)).map(|y| Point {x:self.x1, y:y}).collect()
            }
            Orient::Diagonal=> {
                panic!("Impliment Diagonal get_points")
            }
        }
    }

    pub fn points(&mut self) -> Vec<Point> {
        match self.points {
            None => {
                self.points = Some(self.get_points())
            },
            _ => ()
        }
        let points = self.points.as_ref();
        points.unwrap().to_vec()
    }
}

fn main() {
    let mut lines = input::lines();

    let mut lines : Vec<Line> = lines.filter(|line| !line.as_ref().unwrap().trim().is_empty())
                                .map(|line| line.unwrap().to_string())
                                .map(|line| line.split(" -> ").filter(|seg| !seg.trim().is_empty())
                                                              .map(|seg| seg.to_string())
                                                              .collect::<Vec<String>>())
                                .map(|vals| Line::new(vals[0].as_str(), vals[1].as_str()) )
                                .collect();
    let mut points : Vec<Point> = vec![];
    for (_i, line) in lines.iter_mut().filter(|line| line.orient != Orient::Diagonal).enumerate() {
        points.append(&mut line.points());

    }

    let mut fin : usize = 0;
    for p in HashSet::<Point>::from_iter(points.to_vec()) {
        if points.iter().filter(|&n| *n == p).count() > 1 {
            fin += 1;
        }
    }
    println!("{:?}", fin);
}
