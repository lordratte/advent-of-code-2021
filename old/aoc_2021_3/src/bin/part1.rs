use std::fs::File;
use std::io::{BufRead, BufReader};
use std::env;


fn main() {
    let mut gamma: Vec<isize> = vec![];

    //--------
    let args: Vec<String> = env::args().collect();
    let filename = args.get(1).unwrap_or_else(|| panic!("Please provide file name"));
    let file = File::open(filename).unwrap_or_else(|_| panic!("Couldn't open file {}", filename));
    let reader = BufReader::new(file);
    //--------

    for (_index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        if line.trim().is_empty() {continue;}
        
        
        // Get line as vector of bits. Each bit is -1 if 0 and 1 if 1. For easy summing.
        let curr:Vec<isize> = line.split("")
                                    .filter_map(|b| match b.parse::<isize>() {
                                        Ok(b) => Some(b*2-1),
                                        Err(_) => None
                                    })
                                    .collect();
        if gamma.len() > 0 {
            for (elem, new) in gamma.iter_mut().zip(curr.iter()) {
                *elem += new;
            }
        } else {
            gamma = curr.to_vec();
        }

        println!("{:?}", &curr[..]);
    }
    let (gamma_num, epsilon_num): (usize, usize) = gamma.iter()
        .fold((0, 0), |(resultg, resulte), &d| {
            ((resultg << 1) ^ (if d > 0 {1} else {0}),
            (resulte << 1) ^ (if d < 0 {1} else {0}))
        }); 
    println!("Gamma: {}\nEpsilon: {}", gamma_num, epsilon_num);
    println!("Answer: {:?}", gamma_num*epsilon_num);
    
}
