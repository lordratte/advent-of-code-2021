use std::fs::File;
use std::io::{BufRead, BufReader};
use std::env;


fn common(lines: Vec<Vec<u8>>, invert: bool) -> Vec<u8> {
    let mut gamma: Vec<isize> = vec![];
    for line in lines.iter() {
        if gamma.len() > 0 {
            for (elem, new) in gamma.iter_mut().zip(line.iter()) {
                *elem += (*new as isize)*2-1;
            }
        } else {
            gamma = line.iter().map(|b| (*b as isize)*2-1).collect();
        }
    }
    gamma.iter().map(|&b| if b > 0 {if invert {0} else {1}} else if b < 0 {if invert {1} else {0}} else {if invert {0} else {1}} ).collect()
}

fn get_num(vec: Vec<u8>) -> usize {
    vec.iter().fold(0, |result, &d| {
        (result << 1) ^ usize::from(d)
    })

}

fn main() {
    let mut lines : Vec<Vec<u8>> = vec![];

    //--------
    let args: Vec<String> = env::args().collect();
    let filename = args.get(1).unwrap_or_else(|| panic!("Please provide file name"));
    let file = File::open(filename).unwrap_or_else(|_| panic!("Couldn't open file {}", filename));
    let reader = BufReader::new(file);
    //--------

    for (_index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        if line.trim().is_empty() {continue;}
        // Get line as vector of bits. Each bit is -1 if 0 and 1 if 1. For easy summing.
        lines.push(line.split("")
                      .filter_map(|b| b.parse::<u8>().ok())
                      .collect());
    }

    println!("Gamma: {:?}", common(lines.to_vec(), false));

    let mut keep: Vec<Vec<u8>> = lines.to_vec();
    let mut i = 0;

    loop {
        let com = common(keep.to_vec(), false);
        let c = com[i];
        for l in (0..keep.len()).rev() {
            if keep[l][i] != c {
                keep.remove(l);
            }
        }
        if keep.len() <= 1 {break;}
        i += 1;
    }

    println!("Keep Ox: {:?}", keep);
    let ox_rating: Vec<u8> = keep.first().unwrap().to_vec();

    let mut keep: Vec<Vec<u8>> = lines.to_vec();
    let mut i = 0;

    loop {
        let com = common(keep.to_vec(), true);
        let c = com[i];
        for l in (0..keep.len()).rev() {
            if keep[l][i] != c {
                keep.remove(l);
            }
        }
        if keep.len() <= 1 {break;}
        i += 1;
    }


    println!("Keep Co: {:?}", keep);
   let co_rating: Vec<u8> = keep.first().unwrap().to_vec();
 //  let co_rating: Vec<u8> = vec![];

    //println!("Gamma: {}\nEpsilon: {}", gamma_num, epsilon_num);
    println!("O2: {:?}", get_num(ox_rating.to_vec()));
    println!("CO2: {:?}", get_num(co_rating.to_vec()));
    println!("Answer: {:?}", get_num(ox_rating)*get_num(co_rating));
    
}
