use std::fs::File;
use std::io::{BufReader, BufRead, Lines};
use std::env;

pub fn lines() -> Lines<BufReader<File>> {
    let args: Vec<String> = env::args().collect();
    let filename = args.get(1).unwrap_or_else(|| panic!("Please provide file name"));
    let file = File::open(filename).unwrap_or_else(|_| panic!("Couldn't open file {}", filename));
    BufReader::new(file).lines()
}
