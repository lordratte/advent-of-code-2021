mod input;

const CHUNKSIZE : usize = 5;

fn test_board(steps : &[&str], board : Vec<Vec<&str>>) -> bool {
    for row in board.iter() {
        if row.iter().all(|e| steps.contains(e)) {println!("{:?}", row);return true}
    }

    for i in 0..CHUNKSIZE {
        if board.iter().all(|row| steps.contains(&row[i])) {println!("{:?}", i);return true}
    }

    false
}

fn score_board(steps : &[&str], board : &Vec<Vec<&str>>) -> usize {
    println!("{:?}", board.iter().flatten().filter(|e| !steps.contains(e)).map(|s| s.parse::<usize>().unwrap()).sum::<usize>());
    println!("{:?}", steps.last().unwrap().parse::<usize>().unwrap());
    println!("{:?}", steps.last().unwrap().parse::<usize>().unwrap()  *  board.iter().flatten().filter(|e| !steps.contains(e)).map(|s| s.parse::<usize>().unwrap()).sum::<usize>());

    0
}


fn main() {
    let mut lines = input::lines();
    let steps = lines.next().unwrap().unwrap();
    let steps : Vec<&str> = steps.split(",").collect();

    let lines: Vec<String> = lines.filter_map(|line| if !line.as_ref().unwrap().trim().is_empty() {line.ok()} else {None})
                                            .collect::<Vec<String>>();
    let boards : Vec<Vec<Vec<&str>>> = lines.chunks(CHUNKSIZE)
                                          .map(|chunk| chunk.iter().map(|line| line.split(' ').filter(|c| c != &"").collect()).collect())
                                          .collect();

    for (ind, _ste) in steps.iter().enumerate() {
        let fin = boards.iter()
              .filter(|board| test_board(&steps[..ind+1], board.to_vec()))
              .next();
        match fin {
            Some(f) => {
                score_board(&steps[..ind+1], &f);
                break;
            },
            _ => (),
        }
    }


//        let line = line.unwrap();
//        if line.trim().is_empty() {continue;}
//    println!("{:#?}", boards.to_vec());

}
