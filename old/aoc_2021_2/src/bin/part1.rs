use std::fs::File;
use std::io::{BufRead, BufReader};
use std::env;


fn main() {
    let mut horizontal: isize = 0;
    let mut depth: isize = 0;

    //--------
    let args: Vec<String> = env::args().collect();
    let filename = args.get(1).unwrap_or_else(|| panic!("Please provide file name"));
    let file = File::open(filename).unwrap_or_else(|_| panic!("Couldn't open file {}", filename));
    let reader = BufReader::new(file);
    //--------

    for (_index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        if line.trim().is_empty() {continue;}
        
        match &line.split(" ").collect::<Vec<&str>>()[..] {
            ["forward", num] => { horizontal += num.parse::<isize>().unwrap()},
            ["up", num] => { depth -= num.parse::<isize>().unwrap() },
            ["down", num] => { depth += num.parse::<isize>().unwrap() },
            _ => (),
        }

    }
    println!("Horizontal: {}", horizontal);
    println!("Depth: {}", depth);
    println!("Horizontal: {}", (horizontal * depth) );
}
