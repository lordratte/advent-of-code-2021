use std::cmp::{max, min};
use std::collections::HashMap;

mod input;

const DAYS: isize = 256;

fn main() {
    let lines = input::all();
    let tm = lines.split(",").filter_map(|o| {
        if !o.is_empty() {
            o.trim().parse::<isize>().ok()
        } else {
            None
        }
    });

    let mut timers = HashMap::<isize, usize>::new();

    for t in tm {
        let t = max(min(t, 8), 0);
        if timers.contains_key(&t) {
            timers.insert(t, timers.get(&t).unwrap_or(&0) + 1);
        } else {
            timers.insert(t, 1);
        }
    }

    for day in 1..=DAYS {
        for fish in 0..=8 {
            timers.insert(fish - 1, *timers.get(&fish).unwrap_or(&0));
            timers.insert(fish - 1, *timers.get(&fish).unwrap_or(&0));
        }
        let ready = *timers.get(&-1).unwrap_or(&0);
        timers.insert(6, *timers.get(&6).unwrap_or(&0) + ready);
        timers.insert(8, ready);
        timers.insert(-1, 0);
    }

    println!("{:?}", timers);
    println!("{:?}", timers.into_values().sum::<usize>());
}
