use std::fs::File;
use std::io::{BufRead, BufReader};
use std::env;


fn main() {
    let mut count: u16 = 0;
    let mut last: u16 = 0;
    let args: Vec<String> = env::args().collect();
    let filename = args.get(1).unwrap_or_else(|| panic!("Please provide file name"));

    let file = File::open(filename).unwrap_or_else(|_| panic!("Couldn't open file {}", filename));
    let reader = BufReader::new(file);

    for (index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        if line.trim().is_empty() {continue;}

        if last < line.parse().unwrap() && index != 0 {
            count += 1;
            println!("{} (increased)", line);
        } else {
            println!("{}", line);
        }
        last = line.parse().unwrap();
    }
    println!("Increase count: {}", count);
}
