use std::fs;
use std::io::{BufRead, BufReader};
use std::env;

fn main() {
    let filename = env::args().nth(1).expect("Please provide file name");
    let file =
        fs::read_to_string(&filename).unwrap_or_else(|_| panic!("Couldn't open file {}", filename));

    let mut count: usize = 0;
    let mut lines = file
        .lines()
        .filter(|line| !line.trim().is_empty())
        .map(|line| line.parse().expect("invalid number"));
    let mut last = lines.next().unwrap_or(0);
    for this in lines {
        if last < this {
            count += 1;
            println!("{} (increased)", this);
        } else {
            println!("{}", this);
        }
        last = this;
    }
    println!("Increase count: {}", count);
}
