use std::cmp::{max, min};

mod input;

pub fn sigma(num: u128) -> u128 {
    println!("-{}", num);
    match num {
        0  => 0,
        1.. => (1..=num).sum(),
        _ => panic!("{}", num)
    }
}

fn main() {
    let lines = input::all();
    let lines = lines.split(",").filter_map(|o| {
        if !o.is_empty() {
            o.trim().parse::<u128>().ok()
        } else {
            None
        }
    }).collect::<Vec<u128>>();


    let calc = |point : u128|  lines.iter().map(|c| sigma(max(c, &point)-min(c, &point))).sum::<u128>();
    let ans = (*lines.iter().min().unwrap()..=*lines.iter().max().unwrap()).map(calc).min().unwrap();
    println!("{:?}", ans);
}
