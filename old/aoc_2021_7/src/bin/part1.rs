use std::cmp::{max, min};

mod input;

fn main() {
    let lines = input::all();
    let lines = lines.split(",").filter_map(|o| {
        if !o.is_empty() {
            o.trim().parse::<isize>().ok()
        } else {
            None
        }
    }).collect::<Vec<isize>>();


    let calc = |point : isize|  lines.iter().map(|c| (c-point).abs()).sum::<isize>();
    let ans = (*lines.iter().min().unwrap()..=*lines.iter().max().unwrap()).map(calc).min().unwrap();
    println!("{:?}", ans);
}
