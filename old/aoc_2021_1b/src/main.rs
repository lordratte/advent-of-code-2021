use std::fs::File;
use std::io::{BufRead, BufReader};
use std::env;


fn main() {
    let mut count: u16 = 0;
    let mut curr:Vec<u16> = Vec::new();
    let mut last:Vec<u16> = Vec::new();

    //--------
    let args: Vec<String> = env::args().collect();
    let filename = args.get(1).unwrap_or_else(|| panic!("Please provide file name"));
    let file = File::open(filename).unwrap_or_else(|_| panic!("Couldn't open file {}", filename));
    let reader = BufReader::new(file);
    //--------

    for (index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        if line.trim().is_empty() {continue;}

        last = curr.to_vec();
        curr.push(line.parse().unwrap());
        if curr.len() > 3 {
            curr.remove(0);
        }
        let curr_sum:u16 = curr.iter().sum();
        let last_sum:u16 = last.iter().sum();

        if last_sum < curr_sum && index >= 3 {
            count += 1;
            println!("{} (increased)", line);
        } else {
            println!("{}", line);
        }
    }
    println!("Curr: {:?}", curr);
    println!("Last: {:?}", last);
    println!("Increase count: {}", count);
}
